"use strict";

function makeRequest(url, config={method: 'GET'}) {
  var promise = new Promise( function (resolve, reject) {
    let req = new XMLHttpRequest();
    req.addEventListener("load", function(e) {
      if (this.status >= 200 && this.status < 300) {
        resolve(JSON.parse(this.responseText));
      }
      else {
        reject(this.statusText);
      }
    });

    req.addEventListener("error", function(e) { reject(this.statusText); });
    req.addEventListener("abort", function(e) { reject(this.statusText); });
    req.open(config.method, url);
    req.send();
  });

  return promise;
}

function buttonClickHandler(inputElement, targetElement, e) {
  e.preventDefault();
  let url = 'https://api.github.com/search/repositories';
  url += '?q=' + encodeURIComponent(inputElement.value);

  targetElement.classList.remove('red-alert');
  makeRequest(url)
    .then(function(resp) {
      console.log(resp);
      targetElement.innerHTML = makeResultsHtml(resp);
    })
    .catch(function(e) {
      console.log(e);
      targetElement.classList.add('red-alert');
      targetElement.innerHTML = `Some kind of error ${e}`;
    });
}

function makeResultsHtml(resultJSON) {
  if(resultJSON.items.length == 0) {
    return '<li>No resuls found </li>';
  }

  let html =  resultJSON.items.map(x => `<li> ${x.full_name} </li>`).join('\n');
  return html;
}

window.onload = function() {
  let content = document.getElementById('content-section');
  content.classList.add('fadein');

  let queryInput = document.getElementById('query-repo');
  let actionButton = document.getElementById('action-button');
  actionButton.addEventListener('click', (e) => buttonClickHandler(queryInput, content, e));
};
  