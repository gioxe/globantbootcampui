"use strict";

function makeRequest(url, config={method: 'GET'}) {
	var promise = new Promise( function (resolve, reject) {
		var req = new XMLHttpRequest();
		req.addEventListener("load", function(e) {
			console.log(this.status);
			if (this.status >= 200 && this.status < 300) {
				resolve(JSON.parse(this.responseText));
			}
			else {
				reject(this.statusText);
			}
		});
		req.addEventListener("error", function(e) { reject(this.statusText); });
		req.addEventListener("abort", function(e) { reject(this.statusText); });
		req.open(config.method, url);
		req.send();
	});

	return promise;
}

function buttonClickHandler(url, targetElement, e) {
	makeRequest(url)
	.then(function(resp) {
		console.log(resp);
		targetElement.innerHTML = resp.value.joke;
	})
	.catch(function(e) {
		console.log(e);
		targetElement.innerHTML = `Some kind of error ${e}`;
	});
}

window.onload = function() {
	var section = document.getElementsByClassName('hello');
	section[0].style.opacity = 1;     // Show

	var joke = document.getElementsByClassName('joke');
	joke[0].addEventListener('click', (e) => buttonClickHandler('http://api.icndb.com/jokes/random', section[0], e));
};

  

